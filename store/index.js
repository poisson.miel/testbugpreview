export const state = () => ({
  header: {}
});

export const mutations = {
  setHeader(state, value) {
    state.header = value;
  }
};

export const actions = {
  async loadPartials({ commit }) {
    const partials = await this.$prismic.api.query(
      this.$prismic.predicates.at("document.type", "menu")
    );

    const header = partials.results.find(x => x.uid === "header").data;
    commit("setHeader", header);
  }
};
